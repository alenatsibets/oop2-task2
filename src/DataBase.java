public class DataBase {
    private final Train[] trains;
    private int length;

    public DataBase() {
        this.trains = new Train[5];
        this.length = 0;
    }

    public void sortByTrainNumbers() {
        boolean isSorted = false;
        Train buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < trains.length-1; i++) {
                if(trains[i].getTrainNumber() > trains[i+1].getTrainNumber()){
                    isSorted = false;
                    buf = trains[i];
                    trains[i] = trains[i+1];
                    trains[i+1] = buf;
                }
            }
        }
    }
    public void display(int number){
        for (int i = 0; i < 5; i++){
            if (trains[i].getTrainNumber() == number){
                System.out.println(trains[i]);
            }
        }
    }
    public void display(){
        for (int i = 0; i < 5; i++){
            System.out.println(trains[i]);
        }
    }
    public void sortByDestination() {
        boolean isSorted = false;
        Train buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < trains.length-1; i++) {
                if(trains[i].getDestinationName().compareTo(trains[i+1].getDestinationName()) > 0){
                    isSorted = false;

                    buf = trains[i];
                    trains[i] = trains[i+1];
                    trains[i+1] = buf;
                }
                if(trains[i].getDestinationName().compareTo(trains[i+1].getDestinationName()) == 0){
                    if(trains[i].getDepartureTime().compareTo(trains[i+1].getDepartureTime()) > 0){
                        isSorted = false;

                        buf = trains[i];
                        trains[i] = trains[i+1];
                        trains[i+1] = buf;
                    }
                }
            }
        }
    }
    public void add(Train a){
        trains[length] = a;
        length++;
    }
}
