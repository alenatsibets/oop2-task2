import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DataBase base = new DataBase();
        base.add(new Train("Ashmiany", 3, "11 a.m."));
        base.add(new Train("Miensk", 1, "5 p.m."));
        base.add(new Train("Garodnia", 7, "3 a.m."));
        base.add(new Train("Vitiebsk", 2, "10 a.m."));
        base.add(new Train("Ashmiany", 6, "5 p.m."));

        System.out.println("Enter a number:");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        base.display(number);
        System.out.println(" ");

        System.out.println("Sorting by train numbers: ");
        base.sortByTrainNumbers();
        base.display();
        System.out.println(" ");

        System.out.println("Sorting by destinations: ");
        base.sortByDestination();
        base.display();
    }

}